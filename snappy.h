#ifndef _SNAPPY_H_
#define _SNAPPY_H_

#include <inttypes.h>

int snappy_check_header_and_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t));
int snappy_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t));

#endif
