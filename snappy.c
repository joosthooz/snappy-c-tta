// #define DEBUG

#ifdef DEBUG
#include <stdio.h>
#undef DEBUG
#define DEBUG(...) fprintf(stderr, __VA_ARGS__)
#else
#define DEBUG(...)
#endif

#include "snappy.h"

//Non-framed Snappy does not have a header, just try decompressing it
int snappy_check_header_and_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t)) {
  return snappy_decomp(buf, pop, push);
}

int snappy_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t)) {

  // Pop and reconstruct the decompressed length from the preamble.
  uint32_t decomp_size = 0;
  uint8_t shamt = 0;
  while (1) {
    int16_t val = pop();
    if (val < 0) return 1;
    decomp_size += (val & 0x7F) << shamt;
    shamt += 7;
    if (!(val & 0x80)) break;
  }
  DEBUG("decomp=%d\n", decomp_size);

  uint32_t decomp_pos = 0;
  while (decomp_pos < decomp_size) {
    int16_t head = pop();
    if (head < 0) return 2;
    uint8_t typ = head & 3;
    uint8_t arg = head >> 2;

    // Handle literals.
    if (!typ) {

      // Decode element header.
      uint32_t lit_len;
      DEBUG("lit arg=%d\n", arg);
      if (arg < 60) {
        lit_len = arg + 1;
      } else {
        lit_len = 1;
        shamt = 0;
        while (arg-- >= 60) {
          int16_t val = pop();
          if (val < 0) return 3;
          lit_len += ((uint32_t)val << shamt);
          shamt += 8;
        }
      }
      DEBUG("lit len=%d\n", lit_len);

      if (!lit_len) continue;

      // Update position.
      if (decomp_pos + lit_len > decomp_size) {
        return 4;
      }
      decomp_pos += lit_len;

#define PERFORM_LITERAL()  { \
        uint8_t val = pop(); \
        *buf++ = val; \
        push(val); \
  }
      // Copy the literal.
      uint8_t *endp = buf + lit_len;
      while (buf < endp) {
         PERFORM_LITERAL();
      }

//Duff's device
/*
    int n = (lit_len + 3) / 4;
    switch (lit_len % 4) {
    case 0: do { PERFORM_LITERAL();
    case 3:      PERFORM_LITERAL();
    case 2:      PERFORM_LITERAL();
    case 1:      PERFORM_LITERAL();
            } while (buf < endp);
    }
*/
      continue;
    }

    // Decode copy header.
    int16_t head2 = pop();
    if (head2 < 0) return 2;
    uint32_t copy_offs;
    uint8_t copy_len;
    if (typ == 1) {
      copy_len = (arg & 0x07) + 4;
      copy_offs = ((uint16_t)(arg & 0x38) << 5) + (head2 & 0xFF);
    } else {
      copy_len = arg + 1;
      copy_offs = head2;
      shamt = 8;
      for (uint8_t i = 0; i < 3; i++) {
        head2 = pop();
        if (head2 < 0) return 2;
        copy_offs += (uint32_t)head2 << shamt;
        shamt += 8;
        if (typ == 2) break;
      }
    }
    DEBUG("copy typ=%d offs=%08X len=%d\n", typ, copy_offs, copy_len);

    // Update position.
    if (copy_offs > decomp_pos) {
      return 6;
    }
    if (decomp_pos + copy_len > decomp_size) {
      return 4;
    }
    decomp_pos += copy_len;

#define PERFORM_COPY()  { \
  uint8_t val = *(buf - copy_offs); \
  *buf++ = val; \
  push(val); \
  }
    // Perform the copy.
    uint8_t *endp = buf + copy_len;
    while (buf < endp) {
      uint8_t val = *(buf - copy_offs);
      *buf++ = val;
      push(val);
    }

//Duff's device
/*
    int n = (copy_len + 3) / 4;
    switch (copy_len % 4) {
    case 0: do { PERFORM_COPY();
    case 3:      PERFORM_COPY();
    case 2:      PERFORM_COPY();
    case 1:      PERFORM_COPY();
            } while (buf < endp);
    }
*/
  }

  return 0;
}
