#define DEBUG

#ifdef DEBUG
#include <stdio.h>
#undef DEBUG
#define DEBUG(...) fprintf(stderr, __VA_ARGS__)
#else
#define DEBUG(...)
#endif

#include "snappy-framed.h"


int snappy_check_header_and_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t)) {
  DEBUG("Trying to decompress framed snappy\n");

  // Pop and check the magic number
  uint32_t magic1 = (pop()<<24) | (pop()<<16) | (pop()<<8) | pop();
  if (magic1 != 0xff060000) {
    DEBUG("Error; wrong stream identifier 1 (0x%X).", magic1);
    return 1;
  }
  uint32_t magic2 = (pop()<<24) | (pop()<<16) | (pop()<<8) | pop();
  if (magic2 != 0x734e6150) {
    DEBUG("Error; wrong stream identifier 2 (0x%X).", magic2);
    return 1;
  }
  uint32_t magic3 = (pop()<<8) | pop();
  if (magic3 != 0x7059) {
    DEBUG("Error; wrong stream identifier 3 (0x%X).", magic3);
    return 1;
  }

  return snappy_decomp(buf, pop, push);
}

int snappy_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t)) {
  DEBUG("Decompressing framed Snappy\n");

  while (1) {
    uint32_t checksum;
    uint8_t chunk_type = pop();
    uint32_t comp_size = pop() | (pop()<< 8) | (pop()<<16);
    if (comp_size == 0) {
      // Decompressing finished
      // TODO: I don't think Snappy has a trailing chunk of size 0 to mark the end.
      DEBUG("Decompression finished.\n");
      break;
    }
    DEBUG("New chunk, size %d\n", comp_size);

    switch (chunk_type) {
      case 0xff: //another stream identifier (assume the length is correct and ignore)
        for (int i = 0; i < 9; i++) pop();
        break;
      case 0x00: //compressed chunk
        // Pop and ignore checksum
        checksum = pop() | (pop()<< 8) | (pop()<<16) | (pop()<<24);
        // The buf is a pointer sliding through the buffer, so keep it updated
        buf = snappy_decomp_block(buf, pop, push);
        break;
      case 0x01: //uncompressed chunk
        DEBUG("uncompressed block, size=%d\n", comp_size);
        // Pop and ignore checksum
        checksum = pop() | (pop()<< 8) | (pop()<<16) | (pop()<<24);
        // Perform the copy.
        for (int i = 0; i < comp_size - 4; i++) {
          push(pop());
        }
        break;
      default:
        if ((chunk_type & 0x80) == 0) { //reserved unskippable chunk
          DEBUG("Error; unrecognized chunk type (0x%X).\n", chunk_type);
          return 1;
        }
      //reserved skippable chunk: fall-through and handle as padding chunk
      case 0xfe: //padding
        for (int i = 0; i < comp_size; i++) {
          pop();
        }
        break;
    }
  }
  return 0;
}

uint8_t *snappy_decomp_block(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t)) {

  // Pop and reconstruct the decompressed length from the preamble.
  uint32_t decomp_size = 0;
  uint8_t shamt = 0;
  while (1) {
    int16_t val = pop();
    if (val < 0) return (uint8_t*)-1;
    decomp_size += (val & 0x7F) << shamt;
    shamt += 7;
    if (!(val & 0x80)) break;
  }
  DEBUG("decomp=%d\n", decomp_size);

  uint32_t decomp_pos = 0;
  while (decomp_pos < decomp_size) {
    int16_t head = pop();
    if (head < 0) return (uint8_t*)-1;
    uint8_t typ = head & 3;
    uint8_t arg = head >> 2;

    // Handle literals.
    if (!typ) {

      // Decode element header.
      uint32_t lit_len;
      DEBUG("lit arg=%d\n", arg);
      if (arg < 60) {
        lit_len = arg + 1;
      } else {
        lit_len = 1;
        shamt = 0;
        while (arg-- >= 60) {
          int16_t val = pop();
          if (val < 0) return (uint8_t*)-1;
          lit_len += ((uint32_t)val << shamt);
          shamt += 8;
        }
      }
      DEBUG("lit len=%d\n", lit_len);

      if (!lit_len) continue;

      // Update position.
      if (decomp_pos + lit_len > decomp_size) {
        return (uint8_t*)-1;
      }
      decomp_pos += lit_len;

#define PERFORM_LITERAL()  { \
        uint8_t val = pop(); \
        *buf++ = val; \
        push(val); \
  }
      // Copy the literal.
      uint8_t *endp = buf + lit_len;
      while (buf < endp) {
         PERFORM_LITERAL();
      }

//Duff's device
/*
    int n = (lit_len + 3) / 4;
    switch (lit_len % 4) {
    case 0: do { PERFORM_LITERAL();
    case 3:      PERFORM_LITERAL();
    case 2:      PERFORM_LITERAL();
    case 1:      PERFORM_LITERAL();
            } while (buf < endp);
    }
*/
      continue;
    }

    // Decode copy header.
    int16_t head2 = pop();
    if (head2 < 0) return (uint8_t*)-1;
    uint32_t copy_offs;
    uint8_t copy_len;
    if (typ == 1) {
      copy_len = (arg & 0x07) + 4;
      copy_offs = ((uint16_t)(arg & 0x38) << 5) + (head2 & 0xFF);
    } else {
      copy_len = arg + 1;
      copy_offs = head2;
      shamt = 8;
      for (uint8_t i = 0; i < 3; i++) {
        head2 = pop();
        if (head2 < 0) return (uint8_t*)-1;
        copy_offs += (uint32_t)head2 << shamt;
        shamt += 8;
        if (typ == 2) break;
      }
    }
    DEBUG("copy typ=%d offs=%08X len=%d\n", typ, copy_offs, copy_len);

    // Update position.
    if (copy_offs > decomp_pos) {
      return (uint8_t*)-1;
    }
    if (decomp_pos + copy_len > decomp_size) {
      return (uint8_t*)-1;
    }
    decomp_pos += copy_len;

#define PERFORM_COPY()  { \
  uint8_t val = *(buf - copy_offs); \
  *buf++ = val; \
  push(val); \
  }
    // Perform the copy.
    uint8_t *endp = buf + copy_len;
    while (buf < endp) {
      uint8_t val = *(buf - copy_offs);
      *buf++ = val;
      push(val);
    }

//Duff's device
/*
    int n = (copy_len + 3) / 4;
    switch (copy_len % 4) {
    case 0: do { PERFORM_COPY();
    case 3:      PERFORM_COPY();
    case 2:      PERFORM_COPY();
    case 1:      PERFORM_COPY();
            } while (buf < endp);
    }
*/
  }

  return buf;
}
