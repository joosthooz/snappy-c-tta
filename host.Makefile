
.PHONY: all
all: snappy data.compressed
	./snappy < data.compressed > data.decompressed.out
	md5sum data.decompressed data.decompressed.out

snappy: main.c snappy.c
	gcc -g $^ -o snappy

data.compressed: data.decompressed
	snzip -traw < $< > $@

data.decompressed:
	dd if=/dev/urandom of=$@.raw bs=1k count=100
	hexdump -C $@.raw > $@

.PHONY: clean
clean:
	rm -f data.compressed data.decompressed data.decompressed.out data.decompressed.raw snappy
