ADF_FILE=6issue_fc_streamout.adf
OPT=-O3 -g

#run make X86=yesplease to compile for x86
ifndef X86
CFLAGS=-Wall ${OPT} -a ${ADF_FILE} -d -v ${TCE_STREAM} ${ADDRESS_SPACE}
LDFLAGS += -a ${ADF_FILE} -d -v
CC=tcecc
endif

ifndef TCE_STREAM_DISABLED
TCE_STREAM=-DTCE_STREAM
else
ifdef SEPARATE_ADDRESS_SPACE
ADDRESS_SPACE=-DSEPARATE_ADDRESS_SPACE
endif
endif


.PHONY: all
all: snappy snappy_framed data.compressed.sn data.compressed.snfr
	./snappy < data.compressed.sn > data.decompressed.sn.out
	./snappy_framed < data.compressed.snfr > data.decompressed.snfr.out
	md5sum data.decompressed data.decompressed.sn.out
	md5sum data.decompressed data.decompressed.snfr.out

snappy: main.c snappy.c
	${CC} $(OPT) ${CFLAGS} $^ -o $@

snappy_framed: main.c snappy-framed.c
	${CC} $(OPT) ${CFLAGS} $^ -o $@

data.compressed.sn: data.decompressed
	snzip -traw < $< > $@

data.compressed.snfr: data.decompressed
	snzip < $< > $@


data.decompressed:
	dd if=/dev/urandom of=$@.raw bs=1k count=100
	hexdump -C $@.raw > $@

.PHONY: clean
clean:
	rm -f data.compressed data.decompressed data.decompressed.out data.decompressed.raw snappy
